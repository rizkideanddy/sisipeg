@extends('layouts.app')

@section('title')
  Tambah Data Pegawai
@endsection

@section('content')
  <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <div class="section-header-back">
              <a href="{{route('users.index')}}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Tambah Data Pegawai / Users</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="{{route('users.index')}}">Pegawai</div></a>
              <div class="breadcrumb-item active">Tambah Data Pegawai</div>
            </div>
          </div>

          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Silahkan isi data-data berikut ini</h4><br><br>
                  </div>

                  <form
                    action="{{route('users.store')}}"
                    method="POST"
                    enctype="multipart/form-data">
                  
                    @csrf

                    <div class="card-body">
                      
                      @if(session('status'))
                      <div class="alert alert-success alert-dismissible show fade">
                        <div class="alert-body">
                          <button class="close" data-dismiss="alert">
                            <span>×</span>
                          </button>
                          {{session('status')}}
                        </div>
                      </div>
                      @endif

                      <div class="form-group">
                        <label for="name">Nama Pegawai</label>
                        <input type="text" class="form-control" required="" name="name" value="">
                      </div>

                      <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" required="" name="username" value="">
                      </div>

                      <div class="form-group">
                        <label for="foto">Pas Foto</label>
                        <input 
                          type="file" 
                          class="form-control" 
                          required="" 
                          name="foto" 
                          value="">
                      </div>

                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" required="" name="email" value="" placeholder="user@email.com">
                      </div>

                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" required="" name="password" value="" placeholder="password">
                      </div>

                      <div class="form-group">
                        <label for="roles">Roles Akun</label>
                        <div class="selectgroup selectgroup-pills">
                          <label class="selectgroup-item">
                            <input 
                              type="checkbox" 
                              name="roles[]" 
                              value="ADMIN" 
                              class="selectgroup-input"
                              id="ADMIN" 
                              required>
                            <span class="selectgroup-button">ADMIN</span>
                          </label>

                          <label class="selectgroup-item">
                            <input 
                              type="checkbox" 
                              name="roles[]" 
                              value="PEGAWAI" 
                              class="selectgroup-input"
                              id="PEGAWAI" 
                              required>
                            <span class="selectgroup-button">PEGAWAI</span>
                          </label>

                          <label class="selectgroup-item">
                            <input 
                              type="checkbox" 
                              name="roles[]" 
                              value="DIREKTUR" 
                              class="selectgroup-input"
                              id="DIREKTUR" 
                              required>
                            <span class="selectgroup-button">DIREKTUR</span>
                          </label>
                        </div>

                      </div>

                      <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input type="text" class="form-control" required="" name="tempat_lahir" value="">
                      </div>

                      <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir</label>
                        <input type="date" class="form-control" required="" name="tanggal_lahir" value="">
                      </div>

                      <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                          <div class="custom-switches-stacked mt-2">
                            <label class="custom-switch">
                              <input type="radio" name="jenis_kelamin" value="PRIA" class="custom-switch-input" checked="" required>
                              <span class="custom-switch-indicator"></span>
                              <span class="custom-switch-description">PRIA</span>
                            </label>
                            <label class="custom-switch">
                              <input type="radio" name="jenis_kelamin" value="WANITA" class="custom-switch-input" checked="" required>
                              <span class="custom-switch-indicator"></span>
                              <span class="custom-switch-description">WANITA</span>
                            </label>
                          </div>
                      </div>

                      <div class="form-group">
                        <label for="tempat_tinggal">Tempat Tinggal Saat Ini</label>
                        <textarea class="form-control" name="tempat_tinggal id="keterangan"" required></textarea>
                      </div>

                      <div class="form-group">
                        <label for="NIK">NIK</label>
                        <input type="number" name="nik" class="form-control" required>
                      </div>

                      <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="keterangan"" required></textarea>
                      </div>


              
                    </div>

                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan</button>
                    </div>

                  </form>

                </div>
              </>
            </div>
          </div>

        </section>
      </div>
@endsection