@extends('layouts.app')

@section('title')
  Tambah Data Pegawai
@endsection

@section('content')
  <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <div class="section-header-back">
              <a href="{{route('jabatans.index')}}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Edit Data Jabatan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="{{route('jabatans.index')}}">Jabatan</div></a>
              <div class="breadcrumb-item active">Edit Data Jabatan</div>
            </div>
          </div>

          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Silahkan edit data jabatan berikut ini</h4><br><br>
                  </div>

                  <form
                    action="{{route('jabatans.update', [$jabatan->id])}}"
                    method="POST">
                  
                    @csrf

                    <input
                      type="hidden"
                      name="_method"
                      value="PUT">

                    <div class="card-body">
                      
                      @if(session('status'))
                      <div class="alert alert-success alert-dismissible show fade">
                        <div class="alert-body">
                          <button class="close" data-dismiss="alert">
                            <span>×</span>
                          </button>
                          {{session('status')}}
                        </div>
                      </div>
                      @endif

                      <div class="form-group">
                        <label for="name">Nama Jabatan / Divisi</label>
                        <input 
                          type="text" 
                          class="form-control" 
                          required 
                          name="name" 
                          value="{{old('name') ? old('name') : $jabatan->name}}">
                      </div>
                      <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="keterangan"" required>{{{old('keterangan') ? old('keterangan') : $jabatan->keterangan}}}</textarea>
                      </div>
                      <div class="form-group mb-0">
                        <label for="">Gaji (Rp.)</label>
                        <input 
                          type="number" 
                          name="gaji" 
                          class="form-control" 
                          required
                          value="{{old('gaji') ? old('gaji') : $jabatan->gaji}}">
                      </div>
                    </div>

                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan</button>
                    </div>

                  </form>
 
                </div>
              </div>
            </div>
          </div>

        </section>
      </div>
@endsection